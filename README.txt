


USAGE:

==================================================

package main

import "bitbucket.org/agallego/log"

func main() {
     l:= log.New()
     l.Info("this is a info outout")


     l.Debug("this will NOT be on your ouptput file")  

     l.ToggleFlag(log.DEBUG)
     l.Debug("this will be on your ouptput file")
}


OUTPUT IS:

[INFO ][Oct 10 09:31:20.701] this is a info outout
[DEBUG][Oct 10 09:31:20.701] this will be on your ouptput file

===================================================




USING DEFAULT PKG VARIABLES:

===================================================


package main

import "bitbucket.org/agallego/log"

func main() {
	log.Info("this is an info outout")
	log.Info("Defaults are INFO|ERROR|FATAL")
	
	
	log.Debug("this will NOT be on your ouptput file")  
	
	log.ToggleFlag(log.DEBUG)
	log.Debug("this will be on your ouptput file")
}


OUTPUT IS:

[INFO ][Oct 10 09:28:01.431] this is an info outout
[INFO ][Oct 10 09:28:01.431] Defaults are INFO|ERROR|FATAL
[DEBUG][Oct 10 09:28:01.432] this will be on your ouptput file


==================================================


