// small/fast log package that is configurable at runtime. 
package log

import (
	"fmt"
	"io"
	"os"
	"sync"
	"time"
)

const (
	DEBUG = 1 << iota // DEBUG   Priority constant; use Log.Debug()
	INFO              // INFO    Priority constant; use Log.Info ()
	ERROR             // ERROR   Priority constant; use Log.Error()
	PANIC             // PANIC   Priority constant; use Log.Panic()
	FATAL             // FATAL   Priority constant; use Log.Fatal()
)

type Log struct {
	stampFormat      string
	flushImmediately bool
	// extra tag that will appear after the time
	tag    string
	flags  int
	writer io.Writer
	sync.Mutex
}

// Log returned starts with INFO|ERROR|FATAL flags on
func New() *Log {
	l := &Log{
		writer:      os.Stdout,
		flags:       INFO | ERROR | FATAL,
		stampFormat: time.StampMilli,
	}
	return l
}

func (l *Log) Debug(format string, v ...interface{}) {
	if l.IsFlagSet(DEBUG) {
		l.writer.Write([]byte(fmt.Sprintf("[DEBUG][%s] %s %s\n", time.Now().Format(time.StampMilli), l.tag, fmt.Sprintf(format, v...))))
	}
}

func (l *Log) Error(format string, v ...interface{}) {
	if l.IsFlagSet(ERROR) {
		l.writer.Write([]byte(fmt.Sprintf("[ERROR][%s] %s %s\n", time.Now().Format(time.StampMilli), l.tag, fmt.Sprintf(format, v...))))
	}
}

func (l *Log) Info(format string, v ...interface{}) {
	if l.IsFlagSet(INFO) {
		l.writer.Write([]byte(fmt.Sprintf("[INFO ][%s] %s %s\n", time.Now().Format(time.StampMilli), l.tag, fmt.Sprintf(format, v...))))
	}
}

func (l *Log) Fatal(format string, v ...interface{}) {
	if l.IsFlagSet(FATAL) {
		l.writer.Write([]byte(fmt.Sprintf("[FATAL][%s] %s %s\n", time.Now().Format(time.StampMilli), l.tag, fmt.Sprintf(format, v...))))
		os.Exit(1)
	}
}

func (l *Log) Panic(format string, v ...interface{}) {
	if l.IsFlagSet(PANIC) {
		s := fmt.Sprintf("[PANIC][%s] %s %s\n", time.Now().Format(time.StampMilli), l.tag, fmt.Sprintf(format, v...))
		l.writer.Write([]byte(s))
		panic(s)
	}
}

func (l *Log) IsFlagSet(f int) bool {
	return (l.flags & f) == f
}

func (l *Log) ToggleFlag(f int) {
	if l.IsFlagSet(f) {
		// turn off
		l.Lock()
		l.flags &= ^f
		l.Unlock()

	} else {
		// turn on
		l.Lock()
		l.flags |= f
		l.Unlock()
	}
}

func (l *Log) SetWriter(w io.Writer) {
	l.Lock()
	l.writer = w
	l.Unlock()
}

func (l *Log) SetTag(tg string) {
	l.Lock()
	l.tag = fmt.Sprintf("[%s]", tg)
	l.Unlock()
}

func (l *Log) SetTimeStampFormat(format string) {
	l.Lock()
	l.stampFormat = format
	l.Unlock()
}

func (l *Log) SetFlushImmediately(flush bool) {
	l.Lock()
	l.flushImmediately = flush
	l.Unlock()
}
