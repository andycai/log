package log

import (
	"io"
)

var (
	// Offers a default logger. Since the logger is thread-safe it is ok to provide
	// a package default
	defaultLogger = New()
)

func Debug(format string, v ...interface{}) {
	defaultLogger.Debug(format, v...)
}

func Info(format string, v ...interface{}) {
	defaultLogger.Info(format, v...)
}

func Error(format string, v ...interface{}) {
	defaultLogger.Error(format, v...)
}

func Fatal(format string, v ...interface{}) {
	defaultLogger.Fatal(format, v...)
}

func Panic(format string, v ...interface{}) {
	defaultLogger.Panic(format, v...)
}

func IsFlagSet(f int) bool {
	return defaultLogger.IsFlagSet(f)
}

func ToggleFlag(f int) {
	defaultLogger.ToggleFlag(f)
}

func SetWriter(w io.Writer) {
	defaultLogger.SetWriter(w)
}

func SetTag(tg string) {
	defaultLogger.SetTag(tg)
}

func SetTimeStampFormat(format string) {
	defaultLogger.SetTimeStampFormat(format)
}

func SetFlushImmediately(flush bool) {
	defaultLogger.SetFlushImmediately(flush)
}
