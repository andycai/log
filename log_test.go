package log

import (
	"os"
	"testing"
)

func TestToggleFlags(t *testing.T) {
	l := New()
	l.ToggleFlag(DEBUG)
	if !l.IsFlagSet(DEBUG) {
		t.Fatalf("Could not toggle flags")
	}
}

func TestDefaultFlags(t *testing.T) {
	l := New()
	if !l.IsFlagSet(INFO) {
		t.Fatalf("INFO flag was not set during initialization")
	}
}

func TestSetWriter(t *testing.T) {
	l := New()
	l.SetWriter(os.Stdout)
}

func TestOutput(t *testing.T) {
	l := New()
	l.Info("Some information")
	l.Debug("Some debug")

	l.ToggleFlag(DEBUG)
	l.Debug("This debug is supposed to be on output")
}

// test defaultLogger pkg variable

func TestDefaultToggleFlags(t *testing.T) {
	ToggleFlag(DEBUG)
	if !IsFlagSet(DEBUG) {
		t.Fatalf("Could not toggle flags DEBUG for defaultLogger pkg variable")
	}
	ToggleFlag(DEBUG)
}

func TestDefaultDefaultFlags(t *testing.T) {
	if !IsFlagSet(INFO) {
		t.Fatalf("INFO flag was not set during initialization of defaultLogger variable")
	}
}

func TestDefaultSetWriter(t *testing.T) {
	SetWriter(os.Stdout)
}

// TODO(agallego) 
// need to actually write my own writer and then 
// compare output.. flushing to stdout is not a very good test
func TestDefaultOutput(t *testing.T) {
	Info("Some information")
	Debug("Some debug NOT supposed to show")
	SetTag("AlexTag")
	ToggleFlag(DEBUG)
	Debug("This debug is supposed to be on output")
	ToggleFlag(DEBUG)
}
